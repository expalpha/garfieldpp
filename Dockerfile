FROM tmidas/geant4_vmc
WORKDIR /

RUN git clone --depth 1 https://gitlab.cern.ch/garfield/garfieldpp.git
ENV GARFIELD_HOME /garfieldpp
ENV CMAKE_PREFIX_PATH ${GARFIELD_HOME}/install:${CMAKE_PREFIX_PATH}
ENV HEED_DATABASE ${GARFIELD_HOME}/Heed/heed++/database

RUN mkdir build_garfieldpp
WORKDIR build_garfieldpp
RUN cmake3 -DROOTSYS=/usr/lib ../garfieldpp
RUN make -j4
RUN make install

WORKDIR /
CMD /bin/bash